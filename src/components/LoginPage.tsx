"use client";
import Image from "next/image";
import { useForm, SubmitHandler } from "react-hook-form";

type IFormLogin = {
  username: string;
  password: string;
};

export default function LoginPage() {
  const { register, handleSubmit } = useForm<IFormLogin>();

  const onSubmit: SubmitHandler<IFormLogin> = (data) => {
    console.log(data);
  };

  return (
    <div className="h-screen flex items-center justify-center">
      <div className="w-[498px] flex flex-col items-center text-center p-16 bg-white shadow rounded">
        <Image width={100} height={100} src="/leaf-lego.png" alt="logo" />
        <p className="text-2xl text-gray-600 my-4">Login into the system</p>

        <form
          className="w-full flex flex-col"
          onSubmit={handleSubmit(onSubmit)}
        >
          <input
            {...register("username", { required: true })}
            className="login-form-input border-gray-300 hover:border-green-500 focus:border-green-600"
            placeholder="Username"
          />

          <input
            {...register("password", { required: true })}
            className="login-form-input border-gray-300 hover:border-green-500 focus:border-green-600"
            type="password"
            placeholder="Password"
          />

          <button
            className="text-white bg-green-600 py-2 rounded hover:bg-green-700 transition-all"
            type="submit"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
}
